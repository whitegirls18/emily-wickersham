|short description|American actress|
|---- | ----|
| name             | Emily Wickersham|
| image            | Emily Wickersham.jpg|
| birth_name       | Emily Kaiser Wickersham|
| birth_date       | {{Birth date and age|1984|04|26|
| birth_place      | [[Kansas]], United States|
| occupation       | Actress|
| years_active     | 2006–present|
| spouse           | {{marriage|Blake Hanley|2010|2018|reason=divorced|


'''Emily Kaiser Wickersham''' (born April 26, 1984) is an American actress best known for her role as NCIS Special Agent [[List of NCIS characters#Ellie Bishop|Eleanor Bishop]] on ''[[NCIS (TV series)|NCIS]]''.<ref name="NCIS Exclusive: Sopranos Alum Eyed as Ziva's Successor, Debut Set For November">{{cite news|title=NCIS Exclusive: Sopranos Alum Eyed as Ziva's Successor, Debut Set For November|url=http://tvline.com/2013/09/17/emily-wickersham-ncis-season-11-after-ziva|date=September 17, 2013|accessdate=November 5, 2013|newspaper=TV Line}}</ref>

## Early life
Wickersham has [[Austrians|Austrian]] and [[Swedes|Swedish]] ([[Värmland]]) ancestry. She was born in [[Kansas]] but grew up in [[Mamaroneck, New York]].<ref name="aa">{{Cite episode |url=https://www.youtube.com/watch?v=_-MDx1jRA20 |title=Interview with Emily Wickersham |series=The Late Late Show with Craig Ferguson|author=Emily Wickersham|network=CBS|date=December 19, 2013|number=1,846|quote=I'm Austrian and Swedish, born in Kansas.|serieslink=The Late Late Show with Craig Ferguson}}</ref><ref name="bb">{{cite news|url=https://nypost.com/2013/12/07/tvs-biggest-hit-ncis-welcomes-emily-wickersham/|title=TV's biggest hit 'NCIS' welcomes Emily Wickersham|author=Deborah Starr Seibel|date=December 7, 2013|newspaper=New York Post|accessdate=January 15, 2014}}</ref><ref name="marriage">{{cite web|url=https://celebrityping.com/blake-anderson-hanley-husband-of-emily-wickersham/|title=Emily Kaiser Wickersham and Blake Anderson Hanley|last=|first=|date=August 19, 2018|website=Celebrity Ping}}</ref> She attended [[Muhlenberg College]] for two years before dropping out.<ref>{{Cite news|url=http://www.cbs.com/shows/watch_magazine/archive/1002915/|title=NCIS' Emily Wickersham: Ingénew – CBS.com|work=CBS|access-date=January 29, 2018}}</ref>

## Career
In 2013, following the departure of actress [[Cote de Pablo]], Wickersham was cast as [[National Security Agency|NSA]] Analyst Eleanor "Ellie" Bishop in a three-episode arc on ''[[NCIS (TV series)|NCIS]]'', starting November 19, 2013, on [[CBS]]. She was subsequently promoted to series regular.<ref name="Emily Wickersham joins 'NCIS' as series regular">{{cite news|title=Emily Wickersham joins 'NCIS' as series regular|url=http://www.realitytvworld.com/news/emily-wickersham-joins-ncis-as-series-regular-1037688.php|accessdate=November 5, 2013|website=Reality TV World}}</ref><ref>{{cite news|url=http://www.hollywoodreporter.com/live-feed/ncis-promotes-emily-wickersham-series-652818|title='NCIS' Promotes Emily Wickersham to Series Regular|publisher=Hollywoodreporter.com|date=November 4, 2013|accessdate=December 26, 2013|first=Lesley|last=Goldberg}}</ref> Wickersham has also appeared in the feature film ''[[Gone (2012 film)|Gone]]'' and made a number of guest appearances on television.

## Personal life
She married musician<ref>{{cite web |title=EXCLUSIVE PREMIERE: THE CELEBRATORY GLOW OF GHOST LION'S "SHOT THE SUN DOWN" |url=http://atwoodmagazine.com/shot-sun-down-ghost-lion-premiere |website=atwoodmagazine |publisher=atwoodmagazine |accessdate=April 21, 2019|date=November 10, 2016 }}</ref><ref>{{cite web |title=Why star Mark Harmon chose Emily Wickersham to replace Cote de Pablo on NCIS |url=https://www.perthnow.com.au/entertainment/tv/why-star-mark-harmon-chose-emily-wickersham-to-replace-cote-de-pablo-on-ncis-ng-fb147f73d81033c67d290a351e1b9a9b |website=perthnow.com.au |publisher=perthnow |accessdate=April 21, 2019|date=November 4, 2014 }}</ref><ref>{{cite web |title=My Fair Emily |url=https://www.cbs.com/shows/watch_magazine/archive/1005214/my-fair-emily/ |website=cbs.com |publisher=CBS |accessdate=April 21, 2019}}</ref> Blake Hanley on November 23, 2010, on Little Palm Island in the [[Florida Keys]].<ref>{{cite web|url=http://www.palmbeachdailynews.com/lifestyles/society/emily-kaiser-wickersham-and-blake-anderson-hanley/Bj4CRTVDSvQ1DPUYK1oWeI/|title=Emily Kaiser Wickersham and Blake Anderson Hanley|accessdate=March 23, 2017}}</ref> They divorced in December 2018.<ref>{{cite web |title=After Hollywood heartbreak, Hanley lives dream at SunFest |url=https://cbs12.com/news/local/after-hollywood-heartbreak-hanley-lives-dream-at-sunfest |website=cbs12.com |publisher=[[WPEC]]|accessdate=28 May 2019|date=May 6, 2019 }}</ref>

## Filmography
### Film
{| class="wikitable sortable plainrowheaders"
|-
! scope="col" | Year
! scope="col" | Title
! scope="col" | Role
! scope="col" class="unsortable" | Notes
|-
| scope="row" | 2007
| ''[[Gardener of Eden]]''
| Kate
|
|-
| scope="row" | 2008
| ''[[Definitely, Maybe]]
| 1998 Intern
|
|-
| scope="row" | 2009
| ''[[How I Got Lost]]''
| Taylor
|
|-
| scope="row" | 2010
| ''[[Remember Me (2010 film)|Remember Me]]''
| Miami Blonde
|
|-
| scope="row" | 2011
| ''[[I Am Number Four (film)|I Am Number Four]]''
| Nicole
|
|-
| scope="row" | 2012
| ''[[Gone (2012 film)|Gone]]''
| Molly Conway
|
|-
| scope="row" | 2015
| ''Glitch''
| Vanessa
|
|}

### Television
{| class="wikitable sortable plainrowheaders"
|-
! scope="col" | Year
! scope="col" | Title
! scope="col" | Role
! scope="col" class="unsortable" | Notes
|-
| scope="row" | 2006
| ''[[Late Show with David Letterman]]''
| Jules
| 1 episode
|-
| scope="row" | 2006
| ''[[Parco P.I.]]''
| Grace Carr
| "Just Another Pretty Face"
|-
| scope="row" | 2006–07
| ''{{sortname|The|Sopranos}}''
| [[List of characters from The Sopranos – friends and family#Rhiannon Flammer|Rhiannon Flammer]]
| Recurring role, 4 episodes
|-
| scope="row" | 2007
| ''{{sortname|The|Bronx Is Burning}}''
| Suzy Steinbrenner
| "Caught!"
|-
| scope="row" | 2007
| ''{{sortname|The|Gamekillers}}''
| The Girl
| "Marcus & Aja"
|-
| scope="row" | 2007
| ''Mitch Albom's for One More Day''
| Maria Benetto Lang
| TV film
|-
| scope="row" | 2009
| ''[[Taking Chance]]''
| Kelley Phelps
| TV film
|-
| scope="row" | 2009
| ''[[Law & Order: Criminal Intent]]''
| Ceci Madison
| "Major Case"
|-
| scope="row" | 2009
| ''[[Bored to Death]]''
| Emily
| "The Case of the Stolen Skateboard"
|-
| scope="row" | 2009
| ''[[Trauma (U.S. TV series)|Trauma]]''
| Jessica
| "Stuck"
|-
| scope="row" | 2011
| ''[[Gossip Girl]]''
| Leading Lady
| "Yes, Then Zero"
|-
| scope="row" | 2013
| ''{{sortname|The|Bridge|The Bridge (2013 TV series)}}''
| Kate Millwright
| "Rio", "Maria of the Desert", "Old Friends"
|-
| scope="row" | 2013–present
| ''[[NCIS (TV series)|NCIS]]''
| [[List of NCIS characters#Ellie Bishop|Eleanor "Ellie" Bishop]]
| Recurring role season 11 (3 episodes) <br /> Main role: season 11–present (133 episodes)
|-
| scope="row" | 2016
| ''[[NCIS: New Orleans]]''
| [[Ellie Bishop]]
| "Sister City: Part 2"
|}

## References
{{Reflist}}

## External links
{{Commons category}}
* {{IMDb name|2278978}}

{{Authority control}}

{{DEFAULTSORT:Wickersham, Emily}}
[[Category:Living people]]
[[Category:21st-century American actresses]]
[[Category:American television actresses]]
[[Category:Actresses from New York (state)]]
[[Category:American people of Austrian descent]]
[[Category:American people of Swedish descent]]
[[Category:People from Mamaroneck, New York]]
[[Category:1984 births]]
[[Category:Actresses from Kansas]]
